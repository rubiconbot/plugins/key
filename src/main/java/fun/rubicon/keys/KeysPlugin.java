package fun.rubicon.keys;

import fun.rubicon.keys.commands.CommandGenerateKey;
import fun.rubicon.keys.commands.CommandRedeem;
import fun.rubicon.keys.core.KeyManager;
import fun.rubicon.keys.listener.LeaveExecutor;
import fun.rubicon.plugin.Plugin;
import fun.rubicon.plugin.io.db.Cassandra;
import lombok.Getter;

public class KeysPlugin extends Plugin {

    @Getter
    private static KeysPlugin instance;
    @Getter
    private KeyManager keyManager;



    @Override
    public void init() {
        instance = this;
        keyManager = new KeyManager();
        if(getRubicon().getConfig().getBoolean("beta"))
            registerListener(new LeaveExecutor());
        registerCommand(new CommandGenerateKey());
        registerCommand(new CommandRedeem());
    }

    @Override
    public void onEnable() {
        createKeysTable();
        createBetaTable();
    }

    private void createKeysTable() {
        Cassandra.getCassandra().getConnection().executeAsync(
                "CREATE TABLE IF NOT EXISTS keys (" +
                        "key UUID," +
                        "type text," +
                        "primary key (key) " +
                        ");");

    }

    private void createBetaTable() {
        Cassandra.getCassandra().getConnection().executeAsync(
                "CREATE TABLE IF NOT EXISTS beta (" +
                        "user_id bigint," +
                        "primary key (user_id) " +
                        ");");
    }


}
