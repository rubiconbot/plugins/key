package fun.rubicon.keys.commands;

import fun.rubicon.keys.core.KeyType;
import fun.rubicon.keys.entity.Key;
import fun.rubicon.plugin.command.Command;
import fun.rubicon.plugin.command.CommandCategory;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import fun.rubicon.plugin.util.SafeMessage;
import net.dv8tion.jda.core.entities.TextChannel;

public class CommandGenerateKey extends Command {

    public CommandGenerateKey() {
        super(new String[] {"generate"}, CommandCategory.BOT_OWNER, "<beta>", "Generates keys for beta & co");
    }

    @Override
    public Result execute(CommandEvent event, String[] args) {
        if(args.length == 0)
            return sendHelp(event);
        KeyType type;
        try {
            type = KeyType.valueOf(args[0].toUpperCase());
        } catch (IllegalArgumentException e){
            return send(error("Unknown type!", "The specified type is unknown."));
        }
        Key key = new Key(type);
        System.out.println("hey");
        event.getAuthor().openPrivateChannel().complete().sendMessage(success("Generated key!", String.format("Successfully generated `%s` key: `%s`", key.getType(), key.getKey())).build()).queue();
        return null;
    }
}
