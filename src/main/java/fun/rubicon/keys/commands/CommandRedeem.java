package fun.rubicon.keys.commands;

import fun.rubicon.keys.KeysPlugin;
import fun.rubicon.keys.entity.Key;
import fun.rubicon.plugin.command.Command;
import fun.rubicon.plugin.command.CommandCategory;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;

import java.util.UUID;

public class CommandRedeem extends Command {

    public CommandRedeem() {
        super(new String[] {"redeem"}, CommandCategory.UTILITY, "<type>", "Redeems beta or premium keys");
    }

    @Override
    public Result execute(CommandEvent event, String[] args) {
        if(args.length == 0)
            return sendHelp(event);
        Key key = KeysPlugin.getInstance().getKeyManager().getKey(UUID.fromString(args[0]));
        if(key != null) {
            key.redeem(event.getAuthor());
            return send(success("Redeemed", String.format("Successfully redeemed `%s` key", key.getType().toString())));
        } else
            return send(error("Unknown key", "That key does not exists"));
    }
}
