package fun.rubicon.keys.core;

import fun.rubicon.keys.entity.Key;
import fun.rubicon.keys.io.db.KeyAccessor;
import fun.rubicon.plugin.io.db.Cassandra;

import java.util.UUID;

public class KeyManager {

    public Key getKey(UUID key){
        return Cassandra.getCassandra().getMappingManager().createAccessor(KeyAccessor.class).getKey(key).all().get(0).init();
    }
}
