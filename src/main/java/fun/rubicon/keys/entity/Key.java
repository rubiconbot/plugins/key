package fun.rubicon.keys.entity;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.datastax.driver.mapping.annotations.Transient;
import fun.rubicon.keys.core.KeyType;
import fun.rubicon.plugin.io.db.Cassandra;
import fun.rubicon.plugin.io.db.DatabaseEntity;
import lombok.Getter;
import net.dv8tion.jda.core.entities.User;

import java.util.UUID;
import java.util.function.Consumer;

@Table(name = "keys")
public class Key extends DatabaseEntity<Key> {

    @Getter
    @PartitionKey
    private UUID key;
    @Getter
    @Transient
    private KeyType type;
    @Column(name = "type")
    private String typeString;

    public Key(KeyType type) {
        super(Key.class, Cassandra.getCassandra());
        this.key = UUID.randomUUID();
        this.type = type;
        this.typeString = type.toString();
        save();
    }

    public Key(){
        super(Key.class, Cassandra.getCassandra());
    }

    public Key init(){
        this.type = KeyType.valueOf(typeString);
        return this;
    }

    public void redeem(User user) {
        switch (type){
            case BETA:
                PreparedStatement ps = Cassandra.getCassandra().getConnection().prepare(
                        "INSERT INTO beta (user_id)" +
                        "VALUES (?)");
                BoundStatement boundStatement = ps.bind(user.getIdLong());
                Cassandra.getCassandra().getConnection().execute(boundStatement);
        }
        delete();
    }

    @Override
    public void save() {
        save(this, null, null);
    }

    @Override
    public void save(Consumer<Key> onSuccess) {
        save(this, onSuccess, null);
    }

    @Override
    public void save(Consumer<Key> onSuccess, Consumer<Throwable> onError) {
        save(this, onSuccess, onError);
    }

    @Override
    public void delete() {
        delete(this, null, null);
    }

    @Override
    public void delete(Consumer<Key> onSuccess) {
        delete(this, onSuccess, null);
    }

    @Override
    public void delete(Consumer<Key> onSuccess, Consumer<Throwable> onError) {
        delete(this, onSuccess, onError);
    }
}
