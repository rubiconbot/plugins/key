package fun.rubicon.keys.io.db;

import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.Query;
import fun.rubicon.keys.entity.Key;

import java.util.UUID;

@Accessor
public interface KeyAccessor {

    @Query("SELECT * FROM keys WHERE key = :key")
    Result<Key> getKey(@Param("key") UUID key);

}
