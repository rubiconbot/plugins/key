package fun.rubicon.keys.listener;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import fun.rubicon.plugin.io.db.Cassandra;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class LeaveExecutor extends ListenerAdapter {


    @Override
    public void onGuildJoin(GuildJoinEvent event) {
        if(!isUserBetaTester(event.getGuild().getOwner().getUser()))
            event.getGuild().leave().queue();
    }

    private boolean isUserBetaTester(User user){
        PreparedStatement ps = Cassandra.getCassandra().getConnection().prepare("SELECT * FROM beta WHERE user_id = ?");
        BoundStatement boundStatement = ps.bind(user.getIdLong());
        ResultSet rs = Cassandra.getCassandra().getConnection().execute(boundStatement);
        return rs.one() != null;
    }
}
